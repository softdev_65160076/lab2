/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import static com.mycompany.lab2.Lab2.isWin;
import com.sun.source.tree.BreakTree;
import java.util.Scanner;
import javax.swing.text.html.HTML;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char Player = 'X';
    static int row;
    static int col;
    static int count;

    static void printWelcome() {
        System.out.println("Welcome XO");
        System.out.println("This game X always start first");

    }

    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}
    };

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void printTurn() {
        System.out.println(Player + " Turn");
    }

    static void inputRowcol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row, col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] != '-') {
                continue;
            }
            table[row - 1][col - 1] = Player;
            break;

        }
    }

    static void switchPlayer() {
        if (Player == 'X') {
            Player = 'O';
        } else {
            Player = 'X';
        }
    }

    static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkTayeng1()) {
            return true;
        } else if (checkTayeng2()) {
            return true;
        }
        return false;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != Player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != Player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkTayeng1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != Player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkTayeng2() {
        if (table[0][2] == Player && table[1][1] == Player && table[2][0] == Player) {
            return true;
        } else {
            return false;
        }

    }

    static void count() {
        count++;
    }

    static boolean checkDraw() {
        if (count < 9) {
            return false;
        } else {
            return true;
        }
    }

    static void Printdraw() {
        System.out.println("Draw!!!");
    }

    static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    static void printWin() {
        System.out.println(Player + " Win!!!");
    }

    static boolean Conti() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Continuous(Y/N):");
        String conti = kb.next();
        if (conti.equals("Y")) {
            return true;
        } else {
            return false;
        }
    }

    static void retable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }

        }
        count = 0;
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            while (true) {
                printTable();
                printTurn();
                inputRowcol();
                count();
                if (isWin()) {
                    printTable();
                    printWin();
                    break;
                }
                if (isDraw()) {
                    printTable();
                    Printdraw();
                    break;
                }
                switchPlayer();
            }
            retable();
            if(Conti()==false){
                break;
            }
        }System.out.println("End Game!!!");
    }

}
